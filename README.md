# Quadtree

Old implementation of quadtree data structure and a small ui to demonstrate it. 
Can generate gif-files from the tree structure.
Example: abc.gif - data at black dots (0,0), (1,1), (1,2), (50,50), other colours show different levels of the tree.

