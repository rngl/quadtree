
/*
 * Data node structure, contains coordinates stored in the tree.
 */

public class Datanode {

	
	private int x;
	private int y;
	
	
	public Datanode(int x, int y) {
		
		this.x = x;
		this.y = y;
	}
	
	// get X
	public int X() {
		
		return this.x;
	}
	
	// get Y
	public int Y() {
		
		return this.y;
	}
	
	// comparison
	public boolean equals(Datanode other) {
		
		if (this.x == other.X())
			if (this.y == other.Y())
				return true;
		
		return false;
	}
	
}
