import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/*
 * Class for painting an image of the quadtree.
 * 
 */



public class Treeimage {

	private int size;
	private int scale;
	
	
	public Treeimage(int size) {
		
		this.size = size;
		scale = 1;
	}
	
	/*
	 * Increase the picture size with a multiplier.
	 */
	
	public void scale(int multiplier) {
		
		this.scale = multiplier;
	}
	
	/*
	 * Builds a picture of the tree structure and tries to save it into given file.
	 * Starts from given node, could be root, or could be any node from the tree really,
	 * to draw a subtree only.
	 */
	
	public void createImage(File file, Treenode node) {
	
		// Create image where to paint.
		BufferedImage img = new BufferedImage(size * scale, size * scale, BufferedImage.TYPE_3BYTE_BGR);
		Graphics2D gfx = img.createGraphics();
		
		// Call painting algorithm.
		drawNode(node, gfx, 0, 0, size * scale, size * scale, 0);
		
		// Flip y-axis, so (0,0) is at bottom left instead of top left
		img = flip(img);
		
		// Write into file
		try {
			ImageIO.write(img, "gif", file);
		}
		catch (IOException e) {
			
		}
	}
	
	/*
	 * Painting algorithm. 
	 * Params:
	 * 	node 					= node to scan
	 * 	gfx 					= graphics object to draw into
	 *  xMin, yMin, xMax, yMax 	= area of the image to draw into
	 *  level					= depth of the tree
	 *  
	 *   If a node is not null then paint the area it covers with a color
	 *   depending on if the node has data in it or not. Then recursively 
	 *   proceed to the node's children, giving each a quarter of the area
	 *   the node covers.
	 *   
	 */
	
	private void drawNode(Treenode node, Graphics2D gfx, int xMin, int yMin, int xMax, int yMax, int level) {
	
		if (node != null) {

			// Number of pixels to draw into each direction.
			int pixels = size * scale;
			
			// Adjusts the pixel amount to be suitable based on level.
			for (int i = 0; i < level; i++) 
				pixels = pixels / 2;
			
			if (node.getData() == null)
				color(level, gfx, 0);
			else
				color(level, gfx, 1);
			
			// Paint the area
			gfx.fill(new Rectangle(xMin, yMin, pixels, pixels));

			// Adjust pixels and level for children
			pixels = pixels / 2;
			level++;

			// Proceed to children
			drawNode(node.getSW(), gfx, xMin, yMin, xMax - pixels, yMax - pixels, level);
			drawNode(node.getNW(), gfx, xMin, yMax - pixels, xMax - pixels, yMax, level);
			drawNode(node.getSE(), gfx, xMax - pixels, yMin, xMax, yMax - pixels, level);
			drawNode(node.getNE(), gfx, xMax - pixels, yMax - pixels, xMax, yMax, level);
		}
	}
		
	
	/*
	 * Set colors to make your eyes bleed. 
	 * x == 1 means node has data in it so set color to black
	 */
	
	private void color(int level, Graphics2D gfx, int x) {
			
		if (x == 1)
			gfx.setColor(Color.black);
		else {
			switch(level) {
			
				case 0:	
					gfx.setColor(Color.white);
					break;
				case 1:
					gfx.setColor(Color.blue);
					break;
				case 2:
					gfx.setColor(Color.white);
					break;
				case 3:
					gfx.setColor(Color.cyan);
					break;
				case 4:
					gfx.setColor(Color.white);
					break;
				case 5:
					gfx.setColor(Color.red);
					break;
				case 6:
					gfx.setColor(Color.orange);
					break;
				case 7:
					gfx.setColor(Color.yellow);
					break;
				default:
					gfx.setColor(Color.white);
					break;
			}
		}
	}
		
	
	/*
	 * http://www.javalobby.org/articles/ultimate-image/#9
	 * 
	 * Flips Y-axis around to make image more sensible for the viewer.
	 */
	
	private BufferedImage flip(BufferedImage img) {
		
		BufferedImage flip = new BufferedImage(img.getWidth(), img.getHeight(),
							img.getColorModel().getTransparency());
		Graphics2D g = flip.createGraphics();
		g.drawImage(img, 0, 0, img.getWidth(), img.getHeight(), 0,
				img.getHeight(), img.getWidth(), 0, null);
		return flip;
		
	}
	
}
