/*
 * Magical Quadtree.
 * 
 * 
 *
 */


public class Quadtree {

	private Treenode root;
	private int child = 0; // helper for insertion, to know which child the new node is.
	private static int MASK = 0x80; //1000 0000, root level mask
	
	
	public Quadtree() {
		
		root = new Treenode();
		root.setMask(MASK);
		
	}
	
	/*
	 * Run insertion on root.
	 */
	
	public void insert(Datanode data) {
		
		insertToNode(data, root, root);
		
	}
	
	/*
	 * Insertion algorithm.
	 * 
	 * When entering a node in the tree,
	 * 	if it has data in it, run split.
	 * 	if it doesn't have data in it, choose the correct child and
	 *  run insertion on it.
	 *  if it doesn't exist, then create a new node and insert data there.
	 */
	
	private void insertToNode(Datanode data, Treenode node, Treenode temp) {

		if (node == null) {
			
			 node = new Treenode();
			 node.setData(data);
			 node.setFather(temp);
			 // set bit mask one level 'deeper'
			 node.setMask(temp.getMask() >> 1);

			 // use cheat provided by chooseChild to know which child this node is.
			 if (child == 1)
				 temp.setNW(node);
			 else if (child == 2)
				 temp.setNE(node);
			 else if (child == 3)
				 temp.setSW(node);
			 else if (child == 4)
				 temp.setSE(node);
		}
		else if (node.getData() == null) {
			
			insertToNode(data, chooseChild(data, node), node);
		}
		else {
		
			split(data, node);
		}
	}
	
	/*
	 * Split, happens when trying to insert into a node that already 
	 * has data in it. Removes that data from the node, and runs insertion
	 * algorithm for both the new and old data. 
	 */
	
	private void split(Datanode data, Treenode node) {

		if (!node.getData().equals(data)) {
		
			Datanode data2 = node.getData();
			node.setData(null);
		
			insertToNode(data, node, node);
			insertToNode(data2, node, node);
		}
	}
	
	/*
	 * Run search algorithm starting from root.
	 */
	
	public Treenode find(Datanode data) {
		
		return findnode(data, root);
	}
	
	/*
	 * Search algorithm.
	 * 
	 * Goes down in the tree, selecting the path based on active bits.
	 * If it comes down to a node with data, then compare that data
	 * to the wanted data and report accordingly. If a null treenode is
	 * found, then search failed.
	 * 
	 */
	
	private Treenode findnode(Datanode data, Treenode node) {
		
		if (node != null) {
			if (node.getData() != null) {
			
				if (node.getData().equals(data))
					return node;
				else 
					return null;
			}
			else {
			
				return findnode(data, chooseChild(data, node));
			}
		}
		return null;
	}
	
	/*
	 * Selects the right child by comparing the node's bit mask to
	 * the wanted coordinates.
	 * 
	 */
	
	private Treenode chooseChild(Datanode data, Treenode node) {
		
		if ((data.X() & node.getMask()) > 0) {
			
			if ((data.Y() & node.getMask()) > 0) {
				
				child = 2;
				return node.getNE();
			}
			else {
				
				child = 4;
				return node.getSE();
			}
		}
		else {
			
			if ((data.Y() & node.getMask()) > 0) {
				
				child = 1;
				return node.getNW();
			}
			else {
				
				child = 3;
				return node.getSW();
			}
		}
	}
	

	/*
	 * Deletion
	 * 
	 * Find the node which contains wanted data, then, if found, sever
	 * the link from the node's father to this node, removing it from
	 * the tree in the process. Run reconstruction algorithm on the
	 * father, to ensure that the tree remains in correct form.
	 * 
	 */
	
	public boolean delete(Datanode data) {
		
		Treenode temp = find(data);
		
		if (temp != null) {
			
			for (int i = 0; i < 4; i++) {
				if (temp.getFather().getChildren()[i] != null)
					if (temp.getFather().getChildren()[i].equals(temp)) {
						
						temp.getFather().getChildren()[i] = null;
						
						reconstruct(temp.getFather());
						return true;
				}
			}
		}
		
		return false;
	}
	
	/*
	 * Reconstruction algorithm.
	 * 
	 * Checks how many children, that aren't null, the node has (c).
	 * Also, checks how many of those children have data in them (d).
	 * And takes note of the last child with data in it (n).
	 * 
	 * Then, if the node has a single child (c==1), and that child has
	 * data in it (d==1), swap that data into this node, and delete the
	 * child by killing the link to it and, lastly, run construct on 
	 * this node's father.
	 */
	
	private void reconstruct(Treenode node) {
		
		int c = 0;
		int d = 0;
		int n = 0;
		
		for (int i = 0; i < 4; i++) {
			
			if (node.getChildren()[i] != null) {
				
				if (node.getChildren()[i].getData() != null) {
					d++;
					n = i;
				}
				c++;
			}
		}
		
		if (c == 1 && d == 1 && node.getFather() != null) {
			
			node.setData(node.getChildren()[n].getData());
			node.getChildren()[n] = null;
			reconstruct(node.getFather());
		}
	}
	
	public Treenode getRoot() {
		return root;
	}
	
}
