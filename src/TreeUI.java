import java.io.File;
import java.io.FileNotFoundException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/*
 * Crappy UI
 * 
 */

public class TreeUI {

	
	private Quadtree qt;
	private Treeimage ti;
	private Scanner sc;
	private static int MIN = 0;
	private static int MAX = 255;
	private static int SIZE = 256;
	
	public TreeUI() {
		
		qt = new Quadtree();
		ti = new Treeimage(SIZE);
		
		String exit = "X";
		String line = "";
		
		sc = new Scanner(System.in);
		
		while(!line.equals(exit)) {
			process(line);
			line = sc.nextLine();
		}
	}
	
	private void process(String c) {
		
		int x = 0;
		int y = 0;
		
		if (c.equals("1") || c.equals("2") || c.equals("3")) {
			
			System.out.println("Give X and Y coordinates:");
			
			x = sc.nextInt();
			y = sc.nextInt();
		
			if (c.equals("1"))
				insert(x, y);
			if (c.equals("2"))
				delete(x, y);
			if (c.equals("3"))
				search(x, y);
		}
		else if (c.equals("4")) {
			
			System.out.println("Give multiplier:");
			
			x = sc.nextInt();
			
			ti.scale(x);
		}
		else if (c.equals("5")) {
			
			System.out.println("Give filename:");
			String name = sc.next();
			ti.createImage(new File(name), qt.getRoot());
		}
		else if (c.equals("6")) {
			System.out.println("Give filename:");
			String name = sc.next();
			insertFile(name);
		}
		else if (c.equals("7")) {
			System.out.println("Give filename:");
			String name = sc.next();
			deleteFile(name);
		}
		else if (c.equals("8")) {
			clear();
			System.out.println("Tree cleared.");
		}
		else 
			printMenu();
	}
	
	private void clear() {
		qt = new Quadtree();
	}
	
	private void insertFile(String name) {
		
		int x;
		int y;
		
		try {
			Scanner s = new Scanner(new File(name));
			
			try {
				while(true) {
					x = s.nextInt();
					y = s.nextInt();
					
					insert(x,y);
				}
			}
			catch(NoSuchElementException e) {
			}
		}
		catch (FileNotFoundException e) {
			System.out.println("File " + name + " was not found.");
		}
	}
	
	private void deleteFile(String name) {
		
		int x;
		int y;
		
		try {
			Scanner s = new Scanner(new File(name));
			
			try {
				while(true) {
					x = s.nextInt();
					y = s.nextInt();
					
					delete(x,y);
				}
			}
			catch(NoSuchElementException e) {
			}
		}
		catch (FileNotFoundException e) {
			System.out.println("File " + name + " was not found.");
		}
	}
	
	private void insert(int x, int y) {
		
		if (checkNumbers(x, y)) {
			if (qt.find(new Datanode(x, y)) != null)
				System.out.println("X = " + x + " Y = " + y + " is already in tree.");
			else {
				qt.insert(new Datanode(x, y));
				System.out.println("X = " + x + " Y = " + y + " insertion succeeded.");
			}
		}
		else
			System.out.println("X = " + x + " Y = " + y + " not valid coordinates.");
	}
	
	private void search(int x, int y) {
		
		if (checkNumbers(x, y)) {
			if (qt.find(new Datanode(x, y)) != null)
				System.out.println("X = " + x + " Y = " + y + " is in tree.");
			else 
				System.out.println("X = " + x + " Y = " + y + " was not found.");
		}
		else
			System.out.println("X = " + x + " Y = " + y + " not valid coordinates.");
	}
	
	private void delete(int x, int y) {

		if (checkNumbers(x, y)) {
			if (qt.delete(new Datanode(x, y)))
				System.out.println("X = " + x + " Y = " + y + " deleted successfully.");
			else
				System.out.println("X = " + x + " Y = " + y + " deletion failed.");
		}
		else 
			System.out.println("X = " + x + " Y = " + y + " not valid coordinates.");
	}
	
	private boolean checkNumbers(int x, int y) {
		
		if (x < MIN)
			return false;
		if (y < MIN)
			return false;
		if (x > MAX)
			return false;
		if (y > MAX)
			return false;
		
		return true;
	}
	
	private void printMenu() {
		
		System.out.println("****** Menu *******");
		System.out.println(" 1) = Insert");
		System.out.println(" 2) = Delete");
		System.out.println(" 3) = Search");
		System.out.println(" 4) = Scale");
		System.out.println(" 5) = Create");
		System.out.println(" 6) = Insert(File)");
		System.out.println(" 7) = Delete(File)");
		System.out.println(" 8) = Clear");
		System.out.println(" X) = eXit");
		
	}
	
	
	public static void main (String[] args) {
		
		new TreeUI();
		
	}
	
	
}
