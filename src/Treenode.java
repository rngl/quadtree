
/*
 * Tree node structure, contains info on children, father, data and bit mask 
 * 
 * Children magic bits are:
 * 		NE: x = 1, y = 1
 * 		SE: x = 1, y = 0 
 * 		NW: x = 0, y = 1
 * 		SW: x = 0, y = 0
 */

public class Treenode {

	
	private Datanode data;
	private int mask;
	
	private Treenode father;
	private Treenode children[];
	private static int NUMBER_OF_CHILDREN = 4;
	
	// start with everything at null or 0.
	public Treenode() {
		
		mask = 0;
		data = null;
		father = null;
		children = new Treenode[NUMBER_OF_CHILDREN];
		children[0] = null;
		children[1] = null;
		children[2] = null;
		children[3] = null;
		
	}
	
	// set stuff
	public void setFather(Treenode father) {
		
		this.father = father;
	}
	
	public void setMask(int mask) {
		
		this.mask = mask;
	}
	
	public void setNE(Treenode node) {
		
		children[1] = node;
	}
	
	public void setSE(Treenode node) {
		
		children[3] = node;
	}
	
	public void setNW(Treenode node) {
		
		children[0] = node;
	}
	
	public void setSW(Treenode node) {
		
		children[2] = node;
	}
	
	public void setData(Datanode data) {
		
		this.data = data;
	}
	
	// and get stuff.
	public Treenode getFather() {
		
		return father;
	}
	
	public Treenode getNE() {
		
		return children[1];
	}
	
	public Treenode getSE() {
		
		return children[3];
	}
	
	public Treenode getNW() {
	
		return children[0];
	}
	
	public Treenode getSW() {
	
		return children[2];
	}
	
	public int getMask() {
		
		return mask;
	}
	
	public Datanode getData() {
		
		return data;
	}
	
	public Treenode[] getChildren() {
		
		return children;
	}
}
